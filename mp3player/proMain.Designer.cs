﻿using System;
using System.Windows.Forms;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using libZPlay;

namespace mp3_player
{
    partial class proMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(proMain));
            this.T_playlistEvery = new System.Windows.Forms.ListBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加歌曲ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除歌曲ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MP3files = new System.Windows.Forms.Button();
            this.MP3folders = new System.Windows.Forms.Button();
            this.T_playlist = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TBar_PlayTime = new System.Windows.Forms.TrackBar();
            this.T_Volume = new System.Windows.Forms.TrackBar();
            this.T_Born = new System.Windows.Forms.Label();
            this.P_Born = new System.Windows.Forms.ImageList(this.components);
            this.P_play_pause = new System.Windows.Forms.ImageList(this.components);
            this.P_plpa = new System.Windows.Forms.Label();
            this.laters = new System.Windows.Forms.Label();
            this.nexts = new System.Windows.Forms.Label();
            this.T_VolumeText = new System.Windows.Forms.Label();
            this.T_totaltime = new System.Windows.Forms.Label();
            this.T_currenttime = new System.Windows.Forms.Label();
            this.Del = new System.Windows.Forms.Button();
            this.CPlayMode = new System.Windows.Forms.ComboBox();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TBar_PlayTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Volume)).BeginInit();
            this.SuspendLayout();
            // 
            // T_playlistEvery
            // 
            this.T_playlistEvery.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.T_playlistEvery.ContextMenuStrip = this.contextMenuStrip2;
            this.T_playlistEvery.Font = new System.Drawing.Font("仿宋", 10F);
            this.T_playlistEvery.FormattingEnabled = true;
            this.T_playlistEvery.Location = new System.Drawing.Point(157, 187);
            this.T_playlistEvery.Name = "T_playlistEvery";
            this.T_playlistEvery.Size = new System.Drawing.Size(302, 340);
            this.T_playlistEvery.TabIndex = 0;
            this.T_playlistEvery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.T_playlistEvery_KeyDown);
            this.T_playlistEvery.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.T_playlistEvery_MouseDoubleClick);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加歌曲ToolStripMenuItem,
            this.删除歌曲ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(125, 48);
            // 
            // 添加歌曲ToolStripMenuItem
            // 
            this.添加歌曲ToolStripMenuItem.Name = "添加歌曲ToolStripMenuItem";
            this.添加歌曲ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加歌曲ToolStripMenuItem.Text = "添加歌曲";
            this.添加歌曲ToolStripMenuItem.Click += new System.EventHandler(this.MP3files_Click);
            // 
            // 删除歌曲ToolStripMenuItem
            // 
            this.删除歌曲ToolStripMenuItem.Name = "删除歌曲ToolStripMenuItem";
            this.删除歌曲ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除歌曲ToolStripMenuItem.Text = "删除歌曲";
            this.删除歌曲ToolStripMenuItem.Click += new System.EventHandler(this.Del_Click);
            // 
            // MP3files
            // 
            this.MP3files.Location = new System.Drawing.Point(12, 589);
            this.MP3files.Name = "MP3files";
            this.MP3files.Size = new System.Drawing.Size(115, 23);
            this.MP3files.TabIndex = 1;
            this.MP3files.Text = "添加音乐文件";
            this.MP3files.UseVisualStyleBackColor = true;
            this.MP3files.Click += new System.EventHandler(this.MP3files_Click);
            // 
            // MP3folders
            // 
            this.MP3folders.Location = new System.Drawing.Point(133, 589);
            this.MP3folders.Name = "MP3folders";
            this.MP3folders.Size = new System.Drawing.Size(115, 23);
            this.MP3folders.TabIndex = 2;
            this.MP3folders.Text = "添加音乐文件夹";
            this.MP3folders.UseVisualStyleBackColor = true;
            this.MP3folders.Click += new System.EventHandler(this.MP3folders_Click);
            // 
            // T_playlist
            // 
            this.T_playlist.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.T_playlist.ContextMenuStrip = this.contextMenuStrip1;
            this.T_playlist.Font = new System.Drawing.Font("仿宋", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.T_playlist.FormattingEnabled = true;
            this.T_playlist.Location = new System.Drawing.Point(12, 187);
            this.T_playlist.Name = "T_playlist";
            this.T_playlist.Size = new System.Drawing.Size(127, 340);
            this.T_playlist.TabIndex = 3;
            this.T_playlist.SelectedIndexChanged += new System.EventHandler(this.T_playlist_SelectedIndexChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加列表ToolStripMenuItem,
            this.删除列表ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 48);
            // 
            // 添加列表ToolStripMenuItem
            // 
            this.添加列表ToolStripMenuItem.Name = "添加列表ToolStripMenuItem";
            this.添加列表ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加列表ToolStripMenuItem.Text = "添加列表";
            this.添加列表ToolStripMenuItem.Click += new System.EventHandler(this.添加列表ToolStripMenuItem_Click);
            // 
            // 删除列表ToolStripMenuItem
            // 
            this.删除列表ToolStripMenuItem.Name = "删除列表ToolStripMenuItem";
            this.删除列表ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除列表ToolStripMenuItem.Text = "删除列表";
            this.删除列表ToolStripMenuItem.Click += new System.EventHandler(this.删除列表ToolStripMenuItem_Click);
            // 
            // TBar_PlayTime
            // 
            this.TBar_PlayTime.Location = new System.Drawing.Point(12, 114);
            this.TBar_PlayTime.Maximum = 10000;
            this.TBar_PlayTime.Name = "TBar_PlayTime";
            this.TBar_PlayTime.Size = new System.Drawing.Size(411, 45);
            this.TBar_PlayTime.SmallChange = 0;
            this.TBar_PlayTime.TabIndex = 5;
            this.TBar_PlayTime.TabStop = false;
            this.TBar_PlayTime.TickFrequency = 10;
            this.TBar_PlayTime.TickStyle = System.Windows.Forms.TickStyle.None;
            // 
            // T_Volume
            // 
            this.T_Volume.Location = new System.Drawing.Point(355, 58);
            this.T_Volume.Maximum = 100;
            this.T_Volume.MinimumSize = new System.Drawing.Size(100, 25);
            this.T_Volume.Name = "T_Volume";
            this.T_Volume.Size = new System.Drawing.Size(104, 45);
            this.T_Volume.TabIndex = 6;
            this.T_Volume.TabStop = false;
            this.T_Volume.TickStyle = System.Windows.Forms.TickStyle.None;
            this.T_Volume.Value = 100;
            this.T_Volume.Scroll += new System.EventHandler(this.T_Volume_ValueChanged);
            // 
            // T_Born
            // 
            this.T_Born.AutoSize = true;
            this.T_Born.Cursor = System.Windows.Forms.Cursors.Hand;
            this.T_Born.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T_Born.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.T_Born.ImageIndex = 0;
            this.T_Born.ImageList = this.P_Born;
            this.T_Born.Location = new System.Drawing.Point(329, 60);
            this.T_Born.MinimumSize = new System.Drawing.Size(20, 20);
            this.T_Born.Name = "T_Born";
            this.T_Born.Size = new System.Drawing.Size(20, 20);
            this.T_Born.TabIndex = 7;
            this.T_Born.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.T_Born.Click += new System.EventHandler(this.T_Born_Click);
            // 
            // P_Born
            // 
            this.P_Born.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("P_Born.ImageStream")));
            this.P_Born.TransparentColor = System.Drawing.Color.Transparent;
            this.P_Born.Images.SetKeyName(0, "1.png");
            this.P_Born.Images.SetKeyName(1, "2.png");
            // 
            // P_play_pause
            // 
            this.P_play_pause.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("P_play_pause.ImageStream")));
            this.P_play_pause.TransparentColor = System.Drawing.Color.Transparent;
            this.P_play_pause.Images.SetKeyName(0, "chinaz10.png");
            this.P_play_pause.Images.SetKeyName(1, "chinaz9.png");
            // 
            // P_plpa
            // 
            this.P_plpa.AutoSize = true;
            this.P_plpa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.P_plpa.ImageIndex = 0;
            this.P_plpa.ImageList = this.P_play_pause;
            this.P_plpa.Location = new System.Drawing.Point(145, 48);
            this.P_plpa.MinimumSize = new System.Drawing.Size(32, 32);
            this.P_plpa.Name = "P_plpa";
            this.P_plpa.Size = new System.Drawing.Size(32, 32);
            this.P_plpa.TabIndex = 8;
            this.P_plpa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.P_plpa.Click += new System.EventHandler(this.P_plpa_Click);
            // 
            // laters
            // 
            this.laters.AutoSize = true;
            this.laters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.laters.Image = ((System.Drawing.Image)(resources.GetObject("laters.Image")));
            this.laters.Location = new System.Drawing.Point(97, 48);
            this.laters.MinimumSize = new System.Drawing.Size(32, 32);
            this.laters.Name = "laters";
            this.laters.Size = new System.Drawing.Size(32, 32);
            this.laters.TabIndex = 9;
            this.laters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.laters.Click += new System.EventHandler(this.laters_Click);
            // 
            // nexts
            // 
            this.nexts.AutoSize = true;
            this.nexts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nexts.Image = ((System.Drawing.Image)(resources.GetObject("nexts.Image")));
            this.nexts.Location = new System.Drawing.Point(193, 48);
            this.nexts.MinimumSize = new System.Drawing.Size(32, 32);
            this.nexts.Name = "nexts";
            this.nexts.Size = new System.Drawing.Size(32, 32);
            this.nexts.TabIndex = 10;
            this.nexts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.nexts.Click += new System.EventHandler(this.nexts_Click);
            // 
            // T_VolumeText
            // 
            this.T_VolumeText.AutoSize = true;
            this.T_VolumeText.Location = new System.Drawing.Point(434, 82);
            this.T_VolumeText.MinimumSize = new System.Drawing.Size(25, 12);
            this.T_VolumeText.Name = "T_VolumeText";
            this.T_VolumeText.Size = new System.Drawing.Size(29, 12);
            this.T_VolumeText.TabIndex = 11;
            this.T_VolumeText.Text = "100%";
            // 
            // T_totaltime
            // 
            this.T_totaltime.AutoSize = true;
            this.T_totaltime.Location = new System.Drawing.Point(397, 137);
            this.T_totaltime.MinimumSize = new System.Drawing.Size(40, 12);
            this.T_totaltime.Name = "T_totaltime";
            this.T_totaltime.Size = new System.Drawing.Size(40, 12);
            this.T_totaltime.TabIndex = 12;
            this.T_totaltime.Text = "00:00";
            this.T_totaltime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // T_currenttime
            // 
            this.T_currenttime.AutoSize = true;
            this.T_currenttime.Location = new System.Drawing.Point(12, 137);
            this.T_currenttime.MinimumSize = new System.Drawing.Size(40, 12);
            this.T_currenttime.Name = "T_currenttime";
            this.T_currenttime.Size = new System.Drawing.Size(40, 12);
            this.T_currenttime.TabIndex = 13;
            this.T_currenttime.Text = "00:00";
            // 
            // Del
            // 
            this.Del.Location = new System.Drawing.Point(254, 589);
            this.Del.Name = "Del";
            this.Del.Size = new System.Drawing.Size(115, 23);
            this.Del.TabIndex = 14;
            this.Del.Text = "删除歌曲";
            this.Del.UseVisualStyleBackColor = true;
            this.Del.Click += new System.EventHandler(this.Del_Click);
            // 
            // CPlayMode
            // 
            this.CPlayMode.FormattingEnabled = true;
            this.CPlayMode.Items.AddRange(new object[] {
            "单曲播放",
            "单曲循环",
            "顺序播放",
            "列表循环",
            "随机播放"});
            this.CPlayMode.Location = new System.Drawing.Point(375, 592);
            this.CPlayMode.Name = "CPlayMode";
            this.CPlayMode.Size = new System.Drawing.Size(97, 20);
            this.CPlayMode.TabIndex = 15;
            this.CPlayMode.SelectedIndexChanged += new System.EventHandler(this.CPlayMode_SelectedIndexChanged);
            // 
            // proMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(484, 624);
            this.Controls.Add(this.CPlayMode);
            this.Controls.Add(this.Del);
            this.Controls.Add(this.T_currenttime);
            this.Controls.Add(this.T_totaltime);
            this.Controls.Add(this.T_VolumeText);
            this.Controls.Add(this.nexts);
            this.Controls.Add(this.laters);
            this.Controls.Add(this.P_plpa);
            this.Controls.Add(this.T_Born);
            this.Controls.Add(this.T_Volume);
            this.Controls.Add(this.TBar_PlayTime);
            this.Controls.Add(this.T_playlist);
            this.Controls.Add(this.MP3folders);
            this.Controls.Add(this.MP3files);
            this.Controls.Add(this.T_playlistEvery);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 662);
            this.MinimumSize = new System.Drawing.Size(500, 662);
            this.Name = "proMain";
            this.Text = "西子Mp3播放器";
            this.TransparencyKey = System.Drawing.Color.LightCoral;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.proMain_FormClosing);
            this.Load += new System.EventHandler(this.proMain_Load);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TBar_PlayTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_Volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public void T_playlist_SelectedIndexChanged(object sender, System.EventArgs e)//依据播放列表来显示播放列表下的Mp3
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> mp3list = new List<defaultlist>();
            if (this.T_playlist.SelectedIndex >= 0)
            {
                try
                {
                    cmd.CommandText = string.Format("select DISTINCT iname from defaultlist where typename='{0}'", this.T_playlist.SelectedItem.ToString());
                    mp3list = cmd.ExecuteQuery<defaultlist>();
                    FillWithListBox2(mp3list);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Refresh List Err: %s", ex.Message);
                }
            }
        }

        /*************/
        void iStatus()//Status initializtion
        {
            if (flag == -1 || flag == 0 || flag == 1 || flag == 2)
            {
                this.flag = -1;
                return;
            }
        }

        void T_playlistEvery_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyValue == 0x2E)//Delete Mp3
            {
                delete();
            }
            else if (e.KeyValue == 0xD)//Playing Mp3
            {
                iStatus();
                T_plpa_Click(this,new EventArgs());
            }
        }

        void T_playlistEvery_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)//Playing Mp3
        {
            iStatus();
            T_plpa_Click(this, new EventArgs());
        }

        void proMain_FormClosing(object sender, FormClosingEventArgs e)//Form Closing
        {
            aplaymusic.Close();
            Application.Exit();
        }

        void 删除列表ToolStripMenuItem_Click(object sender, System.EventArgs e)//Del List
        {
            DelList_Click(this, new EventArgs());
        }

        void EndMusic()
        {
            
            if (aplaymusic.Close())
            {
                P_plpa.ImageIndex = 0;
            }
        }

        #endregion

        private System.Windows.Forms.ListBox T_playlistEvery;
        private System.Windows.Forms.Button MP3files;
        private System.Windows.Forms.Button MP3folders;
        public System.Windows.Forms.ListBox T_playlist;
        private System.Windows.Forms.TrackBar TBar_PlayTime;
        private System.Windows.Forms.TrackBar T_Volume;
        private System.Windows.Forms.Label T_Born;
        private System.Windows.Forms.ImageList P_Born;
        private System.Windows.Forms.ImageList P_play_pause;
        private System.Windows.Forms.Label P_plpa;
        private System.Windows.Forms.Label laters;
        private System.Windows.Forms.Label nexts;
        private System.Windows.Forms.Label T_VolumeText;
        private System.Windows.Forms.Label T_totaltime;
        private System.Windows.Forms.Label T_currenttime;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem 添加列表ToolStripMenuItem;
        private ToolStripMenuItem 删除列表ToolStripMenuItem;
        private ContextMenuStrip contextMenuStrip2;
        private ToolStripMenuItem 添加歌曲ToolStripMenuItem;
        private ToolStripMenuItem 删除歌曲ToolStripMenuItem;
        private Button Del;
        private ComboBox CPlayMode;
    }
}

