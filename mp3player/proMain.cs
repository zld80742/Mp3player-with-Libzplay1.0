﻿/***************************************
 * 坡小西
 * 2013.4.28
 * MP3 player
***************************************/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using SQLite;
//using WMPLib;
using libZPlay;

namespace mp3_player
{
    public partial class proMain : Form
    {
        /// <summary>
        /// MP3播放器
        /// </summary>
        
        private ZPlay aplaymusic=new ZPlay();
        const string DbName = "MP3List.db";
        string route;
        private PlayingMode aPMode = PlayingMode.Simple;
        int number;
        int iMusic;

        enum PlayingMode
        {
            Simple = 0,//单曲播放
            SimpleLoop,//单曲循环
            List,//顺序播放
            ListLoop,//列表循环
            Random//随机播放
        }

        public proMain()
        {
            InitializeComponent();
            
        }

        #region delegate&Threading

        /*********************************************/
        public delegate void DelegateChangeT(uint sec);
        private void SetBar(uint sec)
        {
            if (this.InvokeRequired)
                this.Invoke(new DelegateChangeT(BarControl), new object[] { sec });
            else
                this.BarControl(sec);
        }
        private void BarControl(uint sec)
        {
            this.TBar_PlayTime.Value = (int)sec;
        }


        public delegate void DelegateChangeT2(uint min, uint sec);
        private void SetBar2(uint min, uint sec)
        {
            if (this.InvokeRequired)
                this.Invoke(new DelegateChangeT2(TextControl), new object[] { min, sec });
            else
                this.TextControl(min, sec);
        }
        private void TextControl(uint min, uint sec)
        {
            T_currenttime.Text = String.Format("{0:D2}:{1:D2}",min,sec);
        }

        public delegate void SelectIndexChangeDelegate(int index);
        private void SetIndex(int index)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SelectIndexChangeDelegate(ChangeSelectIndex), new object[] { index });
            }
            else
            {
                this.ChangeSelectIndex(index);
            }
        }
        private void ChangeSelectIndex(int index)
        {
            T_playlistEvery.SelectedIndex = index;
        }

        public delegate void CPMSelectedIndexChangeDelegate(int index);
        private void CPMSetIndex(int index)
        {
            if (this.InvokeRequired)
                this.Invoke(new CPMSelectedIndexChangeDelegate(CPMIndex), new object[] { index });
            else
                this.CPMIndex(index);
        }
        private void CPMIndex(int index)
        {
            CPlayMode.SelectedIndex = index;
            //return CPlayMode.SelectedIndex;
        }
        #endregion

        #region ReflahSkill

        public void ReflashSkillPMode()
        {
            CPlayMode.SelectedIndex = 0;
        }

        public void ReflashSkill1()
        {
            List<playlist> mp3list = new List<playlist>();
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);

            cmd.CommandText = "select DISTINCT playinglist from playlist";
            mp3list = cmd.ExecuteQuery<playlist>();
            T_playlist.Items.Clear();
            T_playlist.Items.Add("默认列表");
            foreach (playlist ec in mp3list)
            {
                T_playlist.Items.Add(ec.playinglist);
            }
            T_playlist.SelectedIndex = 0;
        }

        public void ReflashSkill2()
        {
            List<defaultlist> mp3list=new List<defaultlist>();
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);

            cmd.CommandText = String.Format("select DISTINCT iname from defaultlist where typename='{0}'", T_playlist.SelectedItem.ToString());
            mp3list = cmd.ExecuteQuery<defaultlist>();
            T_playlistEvery.Items.Clear();
            foreach (defaultlist ec in mp3list)
            {
                T_playlistEvery.Items.Add(ec.iname);
            }
            
            if (T_playlistEvery.Items.Count == -1)
            {
                T_playlistEvery.SelectedIndex = -1;
            }
            else if (T_playlistEvery.Items.Count >= 0)
            {
                T_playlistEvery.SelectedIndex = 0;
            }
        }

        #endregion

        #region FillWithListBox
        private void FillWithListBox1(List<playlist> mp3list)
        {
            T_playlist.Items.Clear();
            int iItems;

            foreach (playlist cd in mp3list)
            {
                T_playlist.Items.Add(cd.playinglist);
                iItems = T_playlist.Items.Count - 1;
            }
        }


        private void FillWithListBox2(List<defaultlist> mp3list)
        {
            T_playlistEvery.Items.Clear();
            int iItems;

            foreach (defaultlist cd in mp3list)
            {
                T_playlistEvery.Items.Add(cd.iname);
                iItems = T_playlistEvery.Items.Count - 1;
            }
        }
        #endregion

        #region Create SQLite
        private void proMain_Load(object sender, EventArgs e)
        {
            List<playlist> mp3list1 = new List<playlist>();
            List<defaultlist> mp3list2 = new List<defaultlist>();
            route = Directory.GetCurrentDirectory();//变量-相对路径

            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            try
            {
                cmd.CommandText = "create table defaultlist(typename varchar(32),iname varchar(32),path varchar(1024),UNIQUE(typename,iname,path))";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "create table playlist(playinglist varchar(16) UNIQUE)";
                cmd.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.Message);
            }

            
            this.T_playlist.Items.Clear();
            cmd.CommandText = string.Format("select DISTINCT playinglist from playlist");
            mp3list1 = cmd.ExecuteQuery<playlist>();
            FillWithListBox1(mp3list1);
            ReflashSkill1();

            this.T_playlistEvery.Items.Clear();
            cmd.CommandText = string.Format("select DISTINCT iname from defaultlist where typename='{0}'",this.T_playlist.SelectedItem.ToString());
            mp3list2 = cmd.ExecuteQuery<defaultlist>();
            FillWithListBox2(mp3list2);
            ReflashSkill2();

            ReflashSkillPMode();
            //有问题
            //EndMusic();
        }
        #endregion

        #region 添加列表

        private void 添加列表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Addlist_Click(this, EventArgs.Empty);
        }

        private void Addlist_Click(object sender, EventArgs e)
        {
            //什么意思，列表空了就不让加？ 我给你加了个不能删除默认列表的功能
            if (T_playlist.Items.Count <= 0)
            {
                return;
            }
            
            Addlist adForm = new Addlist(this);
            adForm.ShowDialog(this);

        }
        #endregion

        #region 文件打开
        //单文件
        private void MP3files_Click(object sender, EventArgs e)
        {
            List<defaultlist> mp3list = new List<defaultlist>();
            SQLiteConnection sql = new SQLiteConnection(route + "//" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Filter = "MP3(*.mp3)|*.mp3";
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                string text1, text2, text3;
                try
                {
                    string imp3file = openfile.FileName;
                    FileInfo imp3 = new FileInfo(imp3file);

                    text1 = T_playlist.SelectedItem.ToString();
                    text2 = Path.GetFileNameWithoutExtension(imp3.Name);
                    text3 = imp3.FullName;
                    cmd.CommandText=string.Format("insert into defaultlist values('{0}','{1}','{2}')",text1,text2,text3);

                    T_playlist.SelectedItem = text1;
                    T_playlistEvery.Items.Add(text2);
                    cmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
        }

        //文件夹及搜索
        private void MP3folders_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openfolder = new FolderBrowserDialog();
            openfolder.SelectedPath = "";
            SQLiteConnection sql = new SQLiteConnection(route + "//" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> mp3list = new List<defaultlist>();
            string ii;//文件夹路径
            if (openfolder.ShowDialog() == DialogResult.OK)
            {
                string[] imp3filesPath = Directory.GetFiles(openfolder.SelectedPath,"*.MP3",SearchOption.AllDirectories);//文件夹下所有文件文件路径
                ii = openfolder.SelectedPath;
                DirectoryInfo CC = new DirectoryInfo(ii);
                for (int i = 0; i < imp3filesPath.Length; i++)//文件夹下所有文件文件名，用数组存储
                {
                    string n = Path.GetFileNameWithoutExtension(imp3filesPath[i]);
                    cmd.CommandText = string.Format("insert into defaultlist values('{0}','{1}','{2}')", T_playlist.SelectedItem.ToString(), n, imp3filesPath[i]);
                    T_playlistEvery.Items.Add(n);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region volume/喇叭
        private void T_Volume_ValueChanged(object sender, EventArgs e)
        {

            this.T_Volume.Minimum = 0;
            this.T_Volume.Maximum = 100;

            T_VolumeText.Text = "100%";
            T_VolumeText.Text = String.Format("{0}%", T_Volume.Value);

            if (aplaymusic == null)
            {
                return;
            }
            if (T_Volume.Value == 0)
            {
                T_Born.ImageIndex = 1;
            }
            else if (T_Volume.Value != 0)
            {
                T_Born.ImageIndex = 0;
            }
            aplaymusic.SetPlayerVolume(T_Volume.Value, T_Volume.Value);
        }

        private void T_Born_Click(object sender, EventArgs e)
        {
            if (T_Born.ImageIndex == 0)
            {
                T_Born.ImageIndex = 1;
                T_Volume.Value = 0;
                T_VolumeText.Text = "0%";
            }
            else if (T_Born.ImageIndex == 1)
            {
                T_Born.ImageIndex = 0;
                T_Volume.Value = 100;
                T_VolumeText.Text = "100%";
            }
            aplaymusic.SetPlayerVolume(T_Volume.Value, T_Volume.Value);
        }
        #endregion

        #region 控制

        private static bool isPlaying = false;
        private Thread th;

        private void PlayControl()
        {
            //Get the Current Time
            TStreamTime CTime = new TStreamTime();
            TStreamStatus status = new TStreamStatus();

            status.fPlay = true;
            isPlaying = true;

            while (isPlaying && status.fPlay)
            {
                
                aplaymusic.GetPosition(ref CTime);
                SetBar(CTime.sec);
                SetBar2(CTime.hms.minute, CTime.hms.second);
                aplaymusic.GetStatus(ref status);
                System.Threading.Thread.Sleep(100);
                number = (int)CTime.sec;
            }
        }
        #endregion

        #region 线程判断
        private void ThreadJudgement()
        {
            if (th.IsAlive == true)
            {
            }
            else if (th.IsAlive == false)
            {
                th.Start();
            }
        }
        #endregion

        #region Get the length of song
        //Get the length of song
        private void GoS()
        {
            //Get the length of song
            TStreamInfo Ttime = new TStreamInfo();
            aplaymusic.GetStreamInfo(ref Ttime);
            T_totaltime.Text = String.Format("{0:D2}:{1:D2}", Ttime.Length.hms.minute, Ttime.Length.hms.second);

            TBar_PlayTime.Minimum = 0;
            TBar_PlayTime.Maximum = (Int32)Ttime.Length.sec;
            TBar_PlayTime.Value = 0;
        }
        #endregion

        #region  播放/暂停
        /// <summary>
        /// 单文件播放
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void T_plpa_Click(object sender, EventArgs e)//Enter & DoubleClick
        {
            if (flag == -1)
            {
                PlayingStart();
                this.flag = 0;
                return;
            }
        }

        private void PlayingStart()
        {
            MPlay();
            Start();
            if (TPS != null)
            {
                TPS.Abort();
                TPS.Join();
                TPS = null;
            }
            StartPlayBackGroudThread();
        }

        int flag = -1;//停止=-1，开始=0，暂停=1，继续=2

        private void P_plpa_Click(object sender, EventArgs e)//Button
        {
            if (T_playlistEvery.SelectedIndex == -1)
            {
                MessageBox.Show("没有选中歌曲!");
            }
            else if (T_playlistEvery.SelectedIndex >= 0)
            {
                if (flag == -1)
                {
                    PlayingStart();
                    this.flag = 0;
                    return;
                }
                else if (flag == 0)
                {
                    Pause();
                    this.flag = 1;
                    return;
                }
                else if (flag == 1)
                {
                    Resume();
                    this.flag = 2;
                    return;
                }
                else if (flag == 2)
                {
                    Pause();
                    this.flag = 1;
                    return;
                }
            }
        }

        public void MPlay()//定位歌曲
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();
            

           //按钮转换及匹配MP3的位置
           string Tname = T_playlistEvery.SelectedItem.ToString();//T_playlistEvery列表栏歌曲名称
           string Ttypename = T_playlist.SelectedItem.ToString();//T_playlist列表栏播放列表名称

           cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}' and iname='{1}'", Ttypename, Tname);
           ipath = cmd.ExecuteQuery<defaultlist>();

           if (ipath[0] == null)
           {
                MessageBox.Show("打开文件失败!");
           }
           aplaymusic.OpenFile(ipath[0].path.ToString(), TStreamFormat.sfAutodetect);
           GoS();

           isPlaying = false;
           th = new Thread(new ThreadStart(PlayControl));
            
           th.IsBackground = true;
            
        }

        private void Start()//开始
        {
            ThreadJudgement();
            aplaymusic.StartPlayback();
            if (P_plpa.ImageIndex == 0)
            {
                P_plpa.ImageIndex = 1;
            }
        }

        private void Pause()//暂停
        {
            th.Suspend();
            aplaymusic.PausePlayback();
            if (P_plpa.ImageIndex == 1)
            {
                P_plpa.ImageIndex = 0;
            }
        }

        private void Resume()//继续
        {
            th.Resume();
            aplaymusic.ResumePlayback();
            if (P_plpa.ImageIndex == 0)
            {
                P_plpa.ImageIndex = 1;
            }
        }

        #endregion

        #region 删除歌曲
        private void Del_Click(object sender, EventArgs e)
        {
            if (T_playlistEvery.SelectedIndex == -1)
            {
                MessageBox.Show("请选中删除项！");
            }
            else if (T_playlistEvery.SelectedIndex >= 0)
            {
                delete();
            }
        }

        private void delete()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            string ditypename = T_playlist.SelectedItem.ToString();//typename
            string diiname = T_playlistEvery.SelectedItem.ToString();//iname
            int index = T_playlistEvery.SelectedIndex;

            try
            {
                cmd.CommandText = string.Format(@"delete from defaultlist where typename='{0}' and iname='{1}'", ditypename, diiname);
                cmd.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                //MessageBox.Show(ex.Message);
            }
            try
            {
                T_playlistEvery.Items.RemoveAt(index);
            }
            catch(SQLiteException ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 列表删除
        private void DelList_Click(object sender,EventArgs e)
        {
            if (T_playlist.SelectedIndex == -1)
            {
                MessageBox.Show("请选中删除项！");
            }
            else if (T_playlist.SelectedIndex >= 0)
            {
                DelList();
            }
        }

        private void DelList()
        {

            int index = T_playlist.SelectedIndex;
            try
            {
                if (index > 0)
                {
                    SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
                    SQLiteCommand cmd = new SQLiteCommand(sql);
                    string iplaylist = T_playlist.SelectedItem.ToString();
                    cmd.CommandText = string.Format(@"delete from playlist where playinglist='{0}'", iplaylist);
                    cmd.ExecuteNonQuery();
                    T_playlist.Items.RemoveAt(index);
                    if (index > 0)
                    {
                        T_playlist.SelectedIndex = index - 1;
                    }
                    else if (index == 0 && T_playlist.Items.Count > 0)
                    {
                        T_playlist.SelectedIndex = 0;
                    }
                }
                else if (index == 0)
                {
                    MessageBox.Show(this, "You can not delete the default list!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(this, "No Selection of the play list, What the hell are you doing!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion


        //Thread→Playing status
        #region Playing status
        private Thread TPS = null;

        private void StartPlayBackGroudThread()
        {
            while (TPS != null)
                Thread.Sleep(100);
            TPS = new Thread(new ThreadStart(ThreadPlayingStatus));
            TPS.IsBackground = true;
            TPS.Start();
        }

        private void ThreadPlayingStatus()
        {
            while (number != 0)
            {
                Thread.Sleep(250);
                MessageBox.Show(number.ToString());
            }
            //P_plpa.ImageIndex = 0;

            int index = 0;
            int iCount = T_playlistEvery.Items.Count;
            SetIndex(index);
            /*判断下一曲index*/
            /*
            switch (CPlayMode.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    SetIndex(index);
                    break;
            }
            */
            /*
            if (CPlayMode.SelectedIndex == 0)
            {
            }
            else if (CPlayMode.SelectedIndex == 1)
            {
                SetIndex(index);
            }
            else if (CPlayMode.SelectedIndex == 2)
            {
                if (T_playlistEvery.SelectedIndex != iCount - 1)
                {
                    index = index + 1;
                }
            }
            else if (CPlayMode.SelectedIndex == 3)
            {
                if (T_playlistEvery.SelectedIndex != iCount - 1)
                {
                    index = index + 1;
                }
                else if (T_playlistEvery.SelectedIndex == iCount - 1)
                {
                    T_playlistEvery.SelectedIndex = 0;
                    index = 0;
                }
            }
            */
            //SetIndex(index);
            /*调用播放 button函数*/
        }
        #endregion

        //OrderLoop单曲循环
        private void OLoopPlay()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();
            if (T_playlistEvery.SelectedIndex == 0)
            {
                int iCount = T_playlistEvery.Items.Count;
                string Tname = T_playlistEvery.SelectedItems[iCount - 1].ToString();
                string Ttypename = T_playlist.SelectedItem.ToString();

                cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}' and iname='{1}'", Ttypename, Tname);
                ipath = cmd.ExecuteQuery<defaultlist>();
                aplaymusic.OpenFile(ipath[0].path.ToString(), TStreamFormat.sfAutodetect);
            }
            else if (T_playlistEvery.SelectedIndex > 0)
            {
                string Tname = T_playlistEvery.SelectedItems[T_playlistEvery.SelectedIndex - 1].ToString();
                string Ttypename = T_playlist.SelectedItem.ToString();

                cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}' and iname='{1}'", Ttypename, Tname);
                ipath = cmd.ExecuteQuery<defaultlist>();
                aplaymusic.OpenFile(ipath[0].path.ToString(), TStreamFormat.sfAutodetect);
            }
            GoS();
            isPlaying = false;
            th = new Thread(new ThreadStart(PlayControl));
            th.IsBackground = true;
        }

        //RandomLoop
        private void RLoopPlay()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();

            string Ttypename = T_playlist.SelectedItem.ToString();
            cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}'", Ttypename);
            ipath = cmd.ExecuteQuery<defaultlist>();//paths of List

            int iCount = ipath.Count;//Count of List
            Random Rpath = new Random();
            int iSong = Rpath.Next(0, iCount - 1);//Song of Random
            aplaymusic.OpenFile(ipath[iSong].path.ToString(), TStreamFormat.sfAutodetect);

            GoS();
            isPlaying = false;
            th = new Thread(new ThreadStart(PlayControl));
            th.IsBackground = true;
        }

        //PlayingList列表播放
        private void PlayingList()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();

            int iCount = T_playlistEvery.Items.Count;

            if (iCount == 0)
            {
                MessageBox.Show("请添加歌曲");
            }
            else if (iCount > 0)
            {
                string itypename = T_playlist.SelectedItem.ToString();
            }
        }

        //上一首←
        private void Last()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();
            int iCount = T_playlistEvery.Items.Count;
            if (iCount == 0)
            {
                MessageBox.Show("请添加歌曲！");
            }
            else if (iCount > 0)
            {
                if (T_playlistEvery.SelectedIndex == -1)
                {
                    MessageBox.Show("没有选中歌曲!");
                }
                else if (T_playlistEvery.SelectedIndex == 0)
                {
                    string itypename = T_playlist.SelectedItem.ToString();
                    string iname = T_playlistEvery.Items[iCount - 1].ToString();
                    

                    cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}' and iname='{1}'", itypename, iname);
                    ipath = cmd.ExecuteQuery<defaultlist>();

                    if (ipath[0] == null)
                    {
                        MessageBox.Show("打开文件失败!");
                    }
                    aplaymusic.OpenFile(ipath[0].path.ToString(), TStreamFormat.sfAutodetect);
                    GoS();

                    isPlaying = false;
                    th = new Thread(new ThreadStart(PlayControl));
                    th.IsBackground = true;
                }
                else if (T_playlistEvery.SelectedIndex > 0)
                {
                    string itypename = T_playlist.SelectedItem.ToString();
                    string iname = T_playlistEvery.Items[iCount - 1].ToString();

                    cmd.CommandText = string.Format(@"select path from defaultlist where typename='{0}' and iname='{1}'", itypename, iname);
                    ipath = cmd.ExecuteQuery<defaultlist>();

                    if (ipath[0] == null)
                    {
                        MessageBox.Show("打开文件失败!");
                    }
                    aplaymusic.OpenFile(ipath[0].path.ToString(), TStreamFormat.sfAutodetect);
                    GoS();

                    isPlaying = false;
                    th = new Thread(new ThreadStart(PlayControl));
                    th.IsBackground = true;
                }
            }
        }

        //下一首→
        private void Next()
        {
            SQLiteConnection sql = new SQLiteConnection(route + "\\" + DbName);
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<defaultlist> ipath = new List<defaultlist>();
        }

        private void laters_Click(object sender, EventArgs e)
        {
            if (flag == -1)
            {
                MPlay();
                Start();
                this.flag = 0;
                return;
            }
            Last();
        }

        private void nexts_Click(object sender, EventArgs e)
        {
        }

        private void CPlayMode_SelectedIndexChanged(object sender, EventArgs e)
        {
        }


    }

}





public class defaultlist
{
    [MaxLength(32)]
    public string typename { get; set; }//列表分类
    [MaxLength(32)]
    public string iname { get; set; }//MP3名字
    [MaxLength(2048)]
    public string path { get; set; }//PATH路径
}

public class playlist
{
    [MaxLength(16)]
    public string playinglist { get; set; }//匹配列表分类
}