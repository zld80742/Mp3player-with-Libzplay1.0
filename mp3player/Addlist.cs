﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SQLite;

namespace mp3_player
{
    public partial class Addlist : Form
    {
        proMain Alist;
        string FormerAlist;
        public Addlist()
        {
            InitializeComponent();
        }

        public Addlist(proMain Aalist)
        {
            InitializeComponent();
            Alist = Aalist;
            this.Text = "Addlist";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SQLiteConnection sql=new SQLiteConnection("MP3List.db");
            SQLiteCommand cmd = new SQLiteCommand(sql);
            List<playlist> mp3list=new List<playlist>();

            string inew="新建列表";
            try
            {
                //有问题
                if (AddListName.Text == "")
                {
                    int n = 1;
                    foreach (playlist cd in mp3list)
                    {
                        string ilist = inew + n;
                        if (cd.playinglist == ilist)
                        {
                            n = n + 1;
                        }
                        AddListName.Text = inew + n;
                    }
                    cmd.CommandText = string.Format("insert into playlist values('{0}')", AddListName.Text);
                    cmd.ExecuteNonQuery();
                }
                else if (AddListName.Text != "")
                {
                    cmd.CommandText = string.Format("insert into playlist values('{0}')", AddListName.Text);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }
            Alist.ReflashSkill1();
            Alist.Activate();
            this.Dispose();
        }

        private void Addlist_Load(object sender, EventArgs e)
        {

        }
    }
}
